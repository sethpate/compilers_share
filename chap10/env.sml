structure Env =
struct

  type ty = Types.ty
  datatype enventry = VarEntry of {access : Translate.access,
                                   ty : ty}
                    | FunEntry of {level : Translate.level,
                                   label : Temp.label,
                                   formals : ty list, 
                                   result : ty}

  (*
  * Basic environment for Tiger types.
  * Symbol.empty is an empty IntMapTable.
  * Symbol.enter takes a (table, symbol, type).
  * So, this environment contains a map between the
  * symbols for "int" and "string" and their Tiger types.
  *)
  val base_tenv : ty Symbol.table =
    Symbol.enter(
      Symbol.enter(
        Symbol.empty, Symbol.symbol("int"), Types.INT),
      Symbol.symbol("string"),
      Types.STRING)

  (*
  * Basic environment for Tiger variables and functions.
  *)
  val base_tig_functions : (string * ty list * ty) list =
    [("print",[Types.STRING],Types.UNIT),
     ("printi",[Types.INT],Types.UNIT),
     ("flush",[],Types.UNIT),
     ("getchar",[],Types.STRING),
     ("ord",[Types.STRING],Types.INT),
     ("chr",[Types.INT],Types.STRING),
     ("size",[Types.STRING],Types.INT),
     ("substring",[Types.STRING,Types.INT,Types.INT],Types.STRING),
     ("concat",[Types.STRING,Types.STRING],Types.STRING),
     ("not",[Types.INT],Types.INT),
     ("exit",[Types.INT],Types.UNIT)]

  val base_venv : enventry Symbol.table =
    List.foldl
      (fn ((name,formals,result),venv) =>
        let
          val label = Temp.newlabel()
          val level = Translate.newLevel 
                  {parent = Translate.outermost,
                   name = label,
                   formals = 
                     (map (fn (x) => false) formals)}
        in
          Symbol.enter(
            venv,
            Symbol.symbol(name),
            FunEntry {level=level,label=label,
                      formals=formals,result=result})
        end)
        Symbol.empty
        base_tig_functions

end
