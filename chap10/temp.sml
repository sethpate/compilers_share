(* Seth looked at an implementation at github.com/sunchao/tiger *)
signature TEMP =

(* Signature from Appell, p 140 *)
sig
  eqtype temp
  val newtemp : unit -> temp
  structure Table : TABLE sharing type Table.key = temp
  val makestring : temp -> string

  (* recall that a Symbol.symbol is a (string * int) *)
  type label = Symbol.symbol
  val newlabel : unit -> label
  val namedlabel : string -> label

  val reset_counters : unit -> unit (* to clear buffer *)
end

structure Temp : TEMP =
struct

  type temp = int (* satisfies the 'eqtype' from sig *)
  val temp_counter = ref 0 (* infinite number of temps *)

  (* 'returns a new temporary from an infinite set of temps' p. 140 *)
  fun newtemp () = 
    let 
      val new = !temp_counter (* dereference the counter to get value *)
    in 
      temp_counter := new + 1; (* now update the pointer to ++ counter *)
      new
    end

  (* add prefix 'temp' to make them clear *)
  fun makestring (t : temp) : string = "temp" ^ Int.toString(t)

  (* Not too clear on this one.
  * IntMapTable is a 'functor' and I'm not sure what that means.
  * It seems to implement a red black tree from the Hashtable library.
  * The key is a temp and the getInt function is required.
  * See reference in Symbol.sml. *)
  structure Table = IntMapTable(type key = temp
                                fun getInt(n)=n)

  type label = Symbol.symbol (* string * int *)

  val label_counter = ref 0 (* infinite set of labels *)

  (* clears buffers that might have data after running make() *)
  fun reset_counters () = (temp_counter := 0; label_counter := 0)

  (* 'returns a new label from an infinite set of labels' p. 140 *)
  fun newlabel () : label =
    let
      val new = !label_counter
    in
      label_counter := new + 1;
      (* I think symbol just takes a string? *)
      Symbol.symbol ("label " ^ Int.toString(new))
    end

  fun namedlabel (s : string) : label = Symbol.symbol s

end
