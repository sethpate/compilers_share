structure MipsFrame : FRAME = 
struct

  val wordSize = 4 (* In MIPS, word = 32 bits *)
  val n_args_in_reg = 4 (* first four arguments are passed in registers *)

  (* 'Access' refers to where this data needs to be stored;
  * in this case, on the stack or in a register. *)
  datatype access = InFrame of int 
                  | InReg of Temp.temp

  type register = string

  (* here be registers *)
  val FP = Temp.newtemp() (* frame pointer *)
  val SP = Temp.newtemp() (* stack pointer *)
  val RV = Temp.newtemp() (* return value *)
  val RA = Temp.newtemp() (* return address *)
  val ZERO = Temp.newtemp() (* zero register *)
  (* function arguments *)
  val a0 = Temp.newtemp()
  val a1 = Temp.newtemp()
  val a2 = Temp.newtemp()
  val a3 = Temp.newtemp()
  (* caller-saves *)
  val t0 = Temp.newtemp()
  val t1 = Temp.newtemp()
  val t2 = Temp.newtemp()
  val t3 = Temp.newtemp()
  val t4 = Temp.newtemp()
  val t5 = Temp.newtemp()
  val t6 = Temp.newtemp()
  val t7 = Temp.newtemp()
  (* callee-saves *)
  val s0 = Temp.newtemp()
  val s1 = Temp.newtemp()
  val s2 = Temp.newtemp()
  val s3 = Temp.newtemp()
  val s4 = Temp.newtemp()
  val s5 = Temp.newtemp()
  val s6 = Temp.newtemp()
  val s7 = Temp.newtemp()

  (* collections of registers *)
  val specialregs = [FP,SP,RV,RA,ZERO]
  val argregs = [a0,a1,a2,a3]
  val calleesaves = [s0,s1,s2,s3,s4,s5,s6,s7]
  val callersaves = [t0,t1,t2,t3,t4,t5,t6,t7]

  val specialregnames = ["fp","sp","rv","ra","zero"]
  val tempMap = foldl (fn ((k,v),table) => 
                            Temp.Table.enter(table,k,v))
                      Temp.Table.empty 
                      (ListPair.zip (specialregs, specialregnames))

  (* MIPS Frames must track:
  * - the location of formals, ('formals' : access list)
  * - how to view shift those formals inside the frame, (?)
  * - the number of local variables declared, ('n_locals')
  * - the label for this machine code. ('name' : Temp.label) *)
  type frame = {name : Temp.label,
                formals : access list,
                n_locals : int ref} (* ref because we must bump it *)

  (* Implemented from frame.sig *)
  datatype frag = PROC of {body : Tree.stm, frame : frame}
                | STRING of Temp.label * string

  (* Return the name of a frame. *)
  fun name (f : frame) : Temp.label =
    let
      val {name=label,formals=_,n_locals=_} = f
    in
      label
    end

  (* required by Appell's emitproc; still not sure how to use *)
  fun string (l: Temp.label, s: string) : string =
    (print "MipsFrame.string needs better implementation\n"; s)

  (* 'formals' argument is a bool list of whether each variable
  * has escaped and must be allocated on the stack. Must convert
  * this into 'access' type containing where this variable
  * can be accessed. *)
  fun newFrame {name : Temp.label,
                formals : bool list}
                : frame =
    let
      (* 'offset' begins with 'wordSize' and is updated with
      * each variable allocated to the stack (InFrame). *)
      fun bool_to_access(escape :: tail : bool list, 
                         offset    : int) : access list =
        let
          val this_access = 
            (* save all escaped parameters to the stack *)
            if escape 
              then InFrame(offset)
            else 
              if List.length (escape::tail) <= n_args_in_reg
                (* save first four args to registers *)
                then (print "in newFrame; save arg to reg\n"; 
                      InReg(Temp.newtemp()))
              (* save all other args to stack *)
              else (print ("in newFrame; save arg to " 
                            ^ Int.toString(offset) ^ "\n");
                            InFrame(offset))
        in
          (* offset is positive for formal parameters *)
          this_access :: bool_to_access(tail, offset + wordSize)
        end
        | bool_to_access([],offset) = []
    in
      {name=name,
       n_locals=ref 0, (* start with 0 declared locals? *)
       formals=bool_to_access(formals, 0)}
    end

  (* Allocates a new local variable in this frame.
  * Returns a function which is called with a boolean
  * indicating whether the variable has escaped.
  * If true, returns InFrame with offset from frame pointer.
  * If false, returns InReg of Temp.temp. *)
  fun allocLocal ({name,formals,n_locals} : frame) : bool -> access =
    let 
      fun alloc_helper (escape : bool) : access =
        if escape 
          then
             (n_locals := !n_locals + 1; (*side effect, bump local count*)
              print ("alloc local at " 
                     ^ Int.toString(0 - (!n_locals) * wordSize) ^"\n");
             (* offset is negative for local variables *)
             InFrame(0 - (!n_locals) * wordSize))
        else
          InReg(Temp.newtemp())
    in
      alloc_helper
    end

  (* Return the list of formals of a frame. *)
  fun formals (f : frame) : access list =
    let
      val {name=_,formals=return_formals,n_locals=_} = f
    in
      return_formals
    end

  (* 'dummy' version from p. 170 *)
  fun procEntryExit1(f: frame, body : Tree.stm ) : Tree.stm = body

  (* Adds an instruction to tell the register allocator which regs
  * are live at end of function. p 208 *)
  fun procEntryExit2(f: frame, body : Assem.instr list) 
        : Assem.instr list =
        body @
        [Assem.OPER{assem="",src=[RA,SP,ZERO] @ calleesaves,
                    dst=[],jump=SOME[]}]

  (* 'scaffold' version, p 209 *)
  fun procEntryExit3({name,formals,n_locals}, body) =
    {prolog = "PROCEDURE " ^ Symbol.name name ^ "\n",
     body = body,
     epilog = "END " ^ Symbol.name name ^ "\n"}

  (* Appell's 'simplest possible implementation' that does not
  * take into account, for example, static links. p. 165. *)
  fun externalCall(s : string, args : Tree.exp list) : Tree.exp =
    Tree.CALL(Tree.NAME(Temp.namedlabel s), args) 

  (* This can be used to look up the value of a location in memory
  * or in register (as in a 'load' operation). However, when used as
  * the left argument in a MOVE op, it is a 'store' operation, as in
  * MOVE (<reg _or_ frame>, value). *)
  fun exp (acc : access) : Tree.exp -> Tree.exp = 
    case acc of InReg(t) => (fn (discard : Tree.exp) => Tree.TEMP(t))
              | InFrame(k) =>
                     (fn (pt : Tree.exp) => 
                       Tree.MEM(Tree.BINOP(Tree.PLUS,
                                           pt,
                                           Tree.CONST(k))))

end
