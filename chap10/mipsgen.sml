structure MipsGen :> CODEGEN =
struct

structure A = Assem
structure T = Tree
structure Frame : FRAME = MipsFrame

exception tooManyArguments of string

fun codegen (f: Frame.frame) (stm : T.stm) : Assem.instr list =

let val ilist = ref (nil: A.instr list) (* holds instructions for later *)

  (* accumulate, as we did with frags in Translate *)
  fun emit x = ilist := x :: !ilist

  (* need to convert ints to strings more easily *)
  fun int i = Int.toString i

  (* Appell p. 205 *)
  val calldefs = [Frame.RA,Frame.RV]@Frame.callersaves

  fun munchStm(T.SEQ(a,b)) = (munchStm a; munchStm b)
    (* store word -- sw *)
    | munchStm(T.MOVE(T.MEM(T.BINOP(T.PLUS,e1,T.CONST i)),e2)) =
          emit(A.OPER{assem="sw `s0, " ^ int i ^ "(`s1)",
                      src=[munchExp e1, munchExp e2],
                      dst=[],jump=NONE})
    | munchStm(T.MOVE(T.MEM(T.BINOP(T.MINUS,e1,T.CONST i)),e2)) =
          emit(A.OPER{assem="sw `s0, " ^ int (~i) ^ "(`s1)",
                      src=[munchExp e1, munchExp e2],
                      dst=[],jump=NONE})
    | munchStm(T.MOVE(T.MEM(T.BINOP(T.PLUS,T.CONST i,e1)),e2)) =
          emit(A.OPER{assem="sw `s0, " ^ int i ^ "(`s1)",
                      src=[munchExp e1, munchExp e2],
                      dst=[],jump=NONE})
    | munchStm(T.MOVE(T.MEM(T.BINOP(T.MINUS,T.CONST i,e1)),e2)) =
          emit(A.OPER{assem="sw `s0, " ^ int (~i) ^ "(`s1)",
                      src=[munchExp e1, munchExp e2],
                      dst=[],jump=NONE})
    | munchStm(T.MOVE(T.MEM(e1),T.MEM(e2))) =
          emit(A.OPER{assem="sw `s0, 0(`s1)",
                      src=[munchExp e1, munchExp e2],
                      dst=[],jump=NONE})

    (* temp to temp MOVE, handle specially *)
    | munchStm(T.MOVE(T.TEMP(t1),T.TEMP(t2))) =
          emit(A.MOVE{assem="move `d0, `s0",
                      src=t2,
                      dst=t1})

    (* load immediate -- li *)
    | munchStm (T.MOVE(T.TEMP i, T.CONST n)) =
          emit(A.OPER({assem="li `d0, " ^ int n,
                       src=[],dst=[i],jump=NONE}))
    (* load word -- lw *)
    | munchStm(T.MOVE(T.TEMP i,T.MEM(T.BINOP(T.PLUS,e1,T.CONST n)))) =
          emit(A.OPER{assem="lw `d0, " ^ int n ^ "(`s0)",
                      src=[munchExp e1],
                      dst=[i],jump=NONE})
    | munchStm(T.MOVE(T.TEMP i,T.MEM(T.BINOP(T.MINUS,e1,T.CONST n)))) =
          emit(A.OPER{assem="lw `d0, " ^ int (~n) ^ "(`s0)",
                      src=[munchExp e1],
                      dst=[i],jump=NONE})
    | munchStm(T.MOVE(T.TEMP i,T.MEM(T.BINOP(T.PLUS,T.CONST n,e1)))) =
          emit(A.OPER{assem="lw `d0, " ^ int n ^ "(`s0)",
                      src=[munchExp e1],
                      dst=[i],jump=NONE})
    | munchStm(T.MOVE(T.TEMP i,T.MEM(T.BINOP(T.MINUS,T.CONST n,e1)))) =
          emit(A.OPER{assem="lw `d0, " ^ int (~n) ^ "(`s0)",
                      src=[munchExp e1],
                      dst=[i],jump=NONE})
    (* move *)
    | munchStm(T.MOVE(T.TEMP i,e2)) =
          emit(A.OPER{assem="move `d0, `s0",
                      src=[munchExp e2],
                      dst=[i],jump=NONE})
    (* jump -- j *)
    | munchStm(T.JUMP(T.NAME lab, _)) =
          emit(A.OPER{assem="j `j0",
                      src=[],dst=[],jump=SOME([lab])})
    (* jump register -- jr *)
    | munchStm(T.JUMP(e, labels)) =
          emit(A.OPER{assem="jr `s0",
                      src=[munchExp e],dst=[],jump=SOME(labels)})
    (* branch on equal -- beq *)
    | munchStm(T.CJUMP(T.EQ,e1,e2,l1,l2)) =
          emit(A.OPER{assem="beq `s0, `s1, `j0\nj `j1",
                      src=[munchExp e1, munchExp e2],dst=[],jump=SOME([l1,l2])})
    (* branch on not equal -- bne *)
    | munchStm(T.CJUMP(T.NE,e1,e2,l1,l2)) =
          emit(A.OPER{assem="bne `s0, `s1, `j0\nj `j1",
                      src=[munchExp e1, munchExp e2],dst=[],jump=SOME([l1,l2])})
    (* branch on greater than -- bgt *)
    | munchStm(T.CJUMP(T.GT,e1,e2,l1,l2)) =
          emit(A.OPER{assem="bgt `s0, `s1, `j0\nj `j1",
                      src=[munchExp e1, munchExp e2],dst=[],jump=SOME([l1,l2])})
    (* branch on greater than or equal -- bge *)
    | munchStm(T.CJUMP(T.GE,e1,e2,l1,l2)) =
          emit(A.OPER{assem="bge `s0, `s1, `j0\nj `j1",
                      src=[munchExp e1, munchExp e2],dst=[],jump=SOME([l1,l2])})
    (* branch on less than -- blt *)
    | munchStm(T.CJUMP(T.LT,e1,e2,l1,l2)) =
          emit(A.OPER{assem="blt `s0, `s1, `j0\nj `j1",
                      src=[munchExp e1, munchExp e2],dst=[],jump=SOME([l1,l2])})
    (* branch on less than or equal -- ble *)
    | munchStm(T.CJUMP(T.LE,e1,e2,l1,l2)) =
          emit(A.OPER{assem="ble`s0, `s1, `j0\nj `j1",
                      src=[munchExp e1, munchExp e2],dst=[],jump=SOME([l1,l2])})
    (* jump and link from register (function call) -- jal *)
    | munchStm(T.EXP(T.CALL(e,args))) =
      let
        val pairs = map (fn r => (Temp.newtemp(),r)) Frame.callersaves
        fun store (reg,value) = T.MOVE(T.TEMP value, T.TEMP reg)
        fun load (reg,value) = T.MOVE(T.TEMP reg, T.TEMP value)
      in
        (* save the caller saves registers *)
        app (fn (a,r) => munchStm(store (a,r))) pairs;
        emit(A.OPER{assem="jalr `s0",
                    src=munchExp e::munchArgs(0,args),
                    dst=calldefs,jump=NONE});
        (* get the caller saves registers back. Not sure why List.rev. *)
        app (fn (a,r) => munchStm(load (a,r))) (List.rev pairs); ()
      end

    | munchStm(T.LABEL lab) =
          emit(A.LABEL{assem=Symbol.name(lab) ^ ":\n",lab=lab})
    | munchStm(T.EXP e) = (munchExp e; ())

  and munchArgs(i, arg_exp::tail) =
      let val n_arg_regs = List.length Frame.argregs
      in
        if i < n_arg_regs then
          let val dst = List.nth(Frame.argregs,i)
              val src = munchExp(arg_exp)
          in munchStm(T.MOVE(T.TEMP dst,T.TEMP src));
             dst :: munchArgs(i+1,tail)
          end
        else raise tooManyArguments("munchArgs: too many arguments for MIPS")
      end
    | munchArgs(i, nil) = nil

  (* creates a new temp and stores result of "gen" in it, returns temp? *)
  and result (gen) = let val t = Temp.newtemp() in gen t; t end

  (* load word -- lw *)
  and munchExp(T.MEM(T.BINOP(T.PLUS,e,T.CONST i))) =
        result(fn r => emit(A.OPER
              {assem="lw `d0," ^ int i ^ "(`s0)",
               src=[munchExp e], dst=[r], jump=NONE}))
    | munchExp(T.MEM(T.BINOP(T.MINUS,e,T.CONST i))) =
        result(fn r => emit(A.OPER
              {assem="lw `d0, " ^ int (~i) ^ "(`s0)",
               src=[munchExp e], dst=[r], jump=NONE}))
    | munchExp(T.MEM(T.BINOP(T.PLUS,T.CONST i,e))) =
        result(fn r => emit(A.OPER
              {assem="lw `d0, " ^ int i ^ "(`s0)",
               src=[munchExp e], dst=[r], jump=NONE}))
    | munchExp(T.MEM(T.BINOP(T.MINUS,T.CONST i,e))) =
        result(fn r => emit(A.OPER
              {assem="lw `d0, " ^ int (~i) ^ "(`s0)",
               src=[munchExp e], dst=[r], jump=NONE}))
    | munchExp(T.MEM(T.CONST i)) =
        result(fn r => emit(A.OPER
              {assem="lw `d0, " ^ int i ^ "($zero)",
               src=[], dst=[r], jump=NONE}))
    (* add immediate -- addiu (not sure about unsigned here) *)
    | munchExp(T.BINOP(T.PLUS,e,T.CONST i)) =
        result(fn r => emit(A.OPER
              {assem="addiu `d0, `s0, " ^ int i,
               src=[munchExp e], dst=[r], jump=NONE}))
    | munchExp(T.BINOP(T.MINUS,e,T.CONST i)) =
        result(fn r => emit(A.OPER
              {assem="addiu `d0, `s0, " ^ int (~i),
               src=[munchExp e], dst=[r], jump=NONE}))
    | munchExp(T.BINOP(T.PLUS,T.CONST i,e)) =
        result(fn r => emit(A.OPER
              {assem="addiu `d0, `s0, " ^ int i,
               src=[munchExp e], dst=[r], jump=NONE}))
    (* add and subtract -- add, sub *)
    | munchExp(T.BINOP(T.PLUS,e1,e2)) =
        result(fn r => emit(A.OPER
              {assem="add `d0, `s0, `s1",
               src=[munchExp e1, munchExp e2], dst=[r], jump=NONE}))
    | munchExp(T.BINOP(T.MINUS,e1,e2)) =
        result(fn r => emit(A.OPER
              {assem="sub `d0, `s0, `s1",
               src=[munchExp e1, munchExp e2], dst=[r], jump=NONE}))
    (* divide -- div *)
    | munchExp(T.BINOP(T.DIV,e1,e2)) =
        result(fn r => emit(A.OPER
              {assem="div `d0, `s0, `s1",
               src=[munchExp e1, munchExp e2], dst=[r], jump=NONE}))
    (* multiply -- mul *)
    | munchExp(T.BINOP(T.MUL,e1,e2)) =
        result(fn r => emit(A.OPER
              {assem="mul `d0, `s0, `s1",
               src=[munchExp e1, munchExp e2], dst=[r], jump=NONE}))
    (* and immediate -- andi *)
    | munchExp(T.BINOP(T.AND,e,T.CONST i)) =
        result(fn r => emit(A.OPER
              {assem="andi `d0, `s0, " ^ int i,
               src=[munchExp e], dst=[r], jump=NONE}))
    | munchExp(T.BINOP(T.AND,T.CONST i,e)) =
        result(fn r => emit(A.OPER
              {assem="andi `d0, `s0, " ^ int i,
               src=[munchExp e], dst=[r], jump=NONE}))
    (* or immediate -- ori *)
    | munchExp(T.BINOP(T.OR,e,T.CONST i)) =
        result(fn r => emit(A.OPER
              {assem="ori `d0, `s0, " ^ int i,
               src=[munchExp e], dst=[r], jump=NONE}))
    | munchExp(T.BINOP(T.OR,T.CONST i,e)) =
        result(fn r => emit(A.OPER
              {assem="ori `d0, `s0, " ^ int i,
               src=[munchExp e], dst=[r], jump=NONE}))
    (* and *)
    | munchExp(T.BINOP(T.AND,e1,e2)) =
        result(fn r => emit(A.OPER
              {assem="and `d0, `s0, `s1",
               src=[munchExp e1, munchExp e2], dst=[r], jump=NONE}))
    (* or *)
    | munchExp(T.BINOP(T.OR,e1,e2)) =
        result(fn r => emit(A.OPER
              {assem="or `d0, `s0, `s1",
               src=[munchExp e1, munchExp e2], dst=[r], jump=NONE}))

    (* load immediate -- li *)
    | munchExp(T.CONST i) =
        result(fn r => emit(A.OPER
              {assem="li `d0, " ^ int i,
               src=[], dst=[r], jump=NONE}))

    | munchExp(T.MEM(e)) =
        result(fn r => emit(A.OPER
              {assem="lw `d0, 0(`s0)",
               src=[munchExp e], dst=[r], jump=NONE}))

    | munchExp(T.TEMP t) = t

    (* load address -- la *)
    | munchExp(T.NAME lab) =
        result(fn r => emit(A.OPER
              {assem="la `d0, " ^ Symbol.name lab,
               src=[], dst=[r], jump=NONE}))

    (* I'm not sure why this function is duplicated here. Seems like it can't 
    * be right. *)
    | munchExp(T.CALL(e,args)) =
      let
        val pairs = map (fn r => (Temp.newtemp(),r)) Frame.callersaves
        fun store (reg,value) = T.MOVE(T.TEMP value, T.TEMP reg)
        fun load (reg,value) = T.MOVE(T.TEMP reg, T.TEMP value)
      in
        (* save the caller saves registers *)
        app (fn (a,r) => munchStm(store (a,r))) pairs;
        result(fn r => emit(A.OPER{assem="jalr `s0",
                              src=munchExp e::munchArgs(0,args),
                              dst=calldefs,jump=NONE}));
        (* get the caller saves registers back. Not sure why List.rev. *)
        app (fn (a,r) => munchStm(load (a,r))) (List.rev pairs); 
        Frame.RV
      end

in munchStm stm;
   rev(!ilist) (* reverses the order, right? because efficiency *)

end

end
