(* Seth looked at an implementation at github.com/sunchao/tiger *)
structure FindEscape :
sig
  val findEscape : Absyn.exp -> unit
end

=

struct
  type depth = int
  type escEnv = (depth * bool ref) Symbol.table

  (* If a variable is already in the environment,
  * set its escape value to 'false' if the current depth 
  * is not greater than its recorded depth.
  * Otherwise, create a binding in the env table for a variable.
  * Handles SimpleVar, FieldVar, SubscriptVar *)
  fun traverseVar(env : escEnv, 
                  d : depth, 
                  s : Absyn.var): unit =
    let
      fun traVar (Absyn.SimpleVar(symbol,pos)) =
        (case Symbol.look(env,symbol) of
             SOME (depth,escape) => if d <= depth
                                      then escape := false else ()
           | NONE => print ("error travar lookup " 
                            ^ Symbol.name symbol ^ "\n"))
        (* can ignore symbol; only var is relevant? *)
        | traVar (Absyn.FieldVar(var,symbol,pos)) = traverseVar(env,d,var)
        (* can ignore exp, only var is relevant? *)
        | traVar (Absyn.SubscriptVar(var,exp,pos)) = traverseVar(env,d,var)
    in
      traVar s
    end

  and traverseExp(env:escEnv, d:depth, s:Absyn.exp): unit =
    case s of Absyn.VarExp(var) => traverseVar(env,d,var)
            | Absyn.CallExp({args,...}) => 
                foldl (fn (exp,_) => traverseExp (env,d,exp)) () args
            | Absyn.OpExp({left,right,...}) => 
                (traverseExp (env,d,left);
                traverseExp (env,d,right))
            | Absyn.RecordExp({fields,...}) =>
                foldl (fn ((sym,exp,pos),_) => 
                        traverseExp (env,d,exp)) 
                      () fields
            | Absyn.SeqExp(sequence) => 
                foldl (fn ((exp,pos),_) => traverseExp (env,d,exp)) 
                      () sequence
            | Absyn.AssignExp({var,exp,pos}) => 
                (traverseVar(env,d,var);
                traverseExp(env,d,exp))

            | Absyn.IfExp({test,then',else',...}) => 
                (traverseExp(env,d,test);
                traverseExp(env,d,then');
                (case else' of SOME exp => traverseExp (env,d,exp)
                             | NONE => ()))
            | Absyn.WhileExp({test,body,...}) => 
                (traverseExp(env,d,test);
                traverseExp(env,d,body))
            | Absyn.ForExp({var,escape,body,lo,hi,pos}) => 
                let val env' = Symbol.enter(env,var,(d,escape))
                in
                  traverseExp(env,d,lo);
                  traverseExp(env,d,hi);
                  traverseExp(env',d,body)
                end
            | Absyn.LetExp({decs,body,pos}) => 
                traverseExp(traverseDecs(env,d,decs),d,body)
            | Absyn.ArrayExp({typ,size,init,pos}) => 
                (traverseExp(env,d,size);
                traverseExp(env,d,init))
            | Absyn.NilExp => ()
            | Absyn.BreakExp(pos) => ()
            | Absyn.IntExp(n) => ()
            | Absyn.StringExp(s,pos) => ()

  (* Fills out the symbol table with (var_name -> (depth, escape))
  * I think every escape starts out as 'ref true' *)
  and traverseDecs(env : escEnv, d : depth, s : Absyn.dec list) : escEnv =
    let
      fun traDec(Absyn.FunctionDec(decs : Absyn.fundec list),env) =
        let
          fun traSingleDec({name,params,result,body,pos}, env) =
            let 
              val env' = foldl 
                          (fn ({name,escape,typ,pos},env) =>
                            (* declared at child function depth *)
                            Symbol.enter(env,name,(d+1,escape)))
                          env
                          params
            in
              (traverseExp(env',d+1,body); (* important *)
               env')
            end
        in
          foldl traSingleDec env decs
        end
        | traDec(Absyn.TypeDec(type_dec::tail),env) = env
        | traDec(Absyn.VarDec{name,escape,typ,init,pos},env) =
          (traverseExp(env,d,init);
          Symbol.enter(env,name,(d,escape)))
    in
      foldl traDec env s
    end


  (* Creates a new escEnv, searches the expression to map variables
  * to their depths and set their escape booleans. *)
  fun findEscape(prog: Absyn.exp) : unit = 
    let
      val env = Symbol.empty
    in
      traverseExp(env, 0, prog) (* begin at depth 0 *)
    end

end
