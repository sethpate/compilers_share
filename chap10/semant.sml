(*
* Collaboration between Michael Davinroy and Seth Pate.
* Seth consulted an implementation found at github.com/sunchao/tiger.
*) 
signature SEMANT =
sig
  (* return type of transExp *)
  type expty = {exp : Translate.exp, ty : Types.ty}

  type venv = Env.enventry Symbol.table
  type tenv = Env.ty Symbol.table

  val transVar : venv * tenv * Absyn.var -> expty
  val transExp : venv * tenv * Absyn.exp -> expty
  val transDec : venv * tenv * Absyn.dec -> {venv: venv, tenv: tenv,
                                             exp_list: Translate.exp list}
  val transTy :         tenv * Absyn.ty  -> Types.ty

end

structure Semant =
struct

  structure A = Absyn
  structure S = Symbol 
  structure TR = Translate 
  structure TY = Types 

  type expty = {exp : TR.exp, ty : TY.ty}

  val error = ErrorMsg.error

  (* return error_result when we can't find a type for some reason *)
  val error_result = {exp=TR.const_zero,ty=TY.NIL}

  fun typeToStr(ty : TY.ty) : string =
    case ty of TY.NIL => "nil"
             | TY.UNIT => "unit"
             | TY.INT => "int"
             | TY.STRING => "string"
             | TY.RECORD (fields,unique) => "record"
             | TY.ARRAY (typ,unique) => "array"
             | TY.NAME (name, typ) => "name"

  (* easy function when we fail a symbol table lookup *)
  fun badLookup (sym : A.symbol, pos : A.pos) =
    (error pos ("could not find in symbol table: "
                ^ S.name sym ^ "\n");
    error_result)

  (* Confirms that expression evaluates to given type *)
  and expIsType (venv : Env.enventry S.table,
                 tenv : Env.ty S.table,
                 level : TR.level,
                 break : Temp.label,
                 exp : A.exp, ty : TY.ty, pos) : unit =
    let
      val {exp=_,ty=exp_ty} = transExp (venv,tenv,level,break) exp
    in
      checkSameType(exp_ty,ty,pos)
    end

  (* Confirms that type1 is the same TY.ty as type2. *)
  and checkSameType (ty1 : TY.ty, ty2 : TY.ty,
                     pos : A.pos) : unit =

    if (actual_ty (ty1,pos) <> actual_ty (ty2,pos)) then
      (error pos ("type mismatch "^typeToStr(ty1)^" "^typeToStr(ty2)^"\n"); ())
    else ()

  (* Tries to identify the 'atomic' type of a given type. *)
  and actual_ty (ty : TY.ty, pos : A.pos) : TY.ty =
    case ty of 
    (* Look up a NAME type to get its actual type *)
      TY.NAME(symbol,type_ref) =>
        (case (!type_ref) of
          NONE => 
            (error pos "error looking up actual name\n"; TY.NIL)
          |  SOME(typ) => actual_ty(typ,pos))
    (* In other cases, just return the type *)
    | _ => ty

    (* Helper functions for A.OpExp.
    * Operations can be arithmetic (+,-,*,/),
    * comparison (<,>,<=,>=), or equality (=,<>).*)

    (* In Tiger, arithmetic can only be performed on ints *)
  and checkArithmetic (ty1, ty2, pos) : unit = 
    case ty1 of TY.INT =>
      checkSameType(ty1,ty2,pos)
    | _ =>
      (error pos "one arg has wrong type for arithmetic"; ())

    (* int and strings may be compared *)
  and checkComparison (ty1, ty2, pos) = 
    case ty1 of TY.INT =>
      checkSameType(ty1,ty2,pos)
    | TY.STRING =>
      checkSameType(ty1,ty2,pos)
    | _ =>
      (error pos "one arg has wrong type for comparison"; ())

    (* int, strings, records, and arrays may all be tested for equality *)
  and checkEquality (ty1 : TY.ty, ty2, pos) : unit = 
    case ty1 of TY.INT =>
      checkSameType(ty1,ty2,pos)
    | TY.STRING =>
      checkSameType(ty1,ty2,pos)
    | TY.ARRAY(typ,unique) =>
      checkSameType(ty1,ty2,pos)
    | TY.RECORD(field, unique) =>
      checkSameType(ty1,ty2,pos)
    | _ =>
      (error pos "one arg has wrong type for equality"; ())
  (*
  * Top level function returning the trexp function.
  * trexp recurs over an A.exp and returns an expty.
  * 'break' is the label of the 'nearest' while loop,
  * where we should jump to if we find a break statement.
  *)
  and transExp(venv, tenv, level, break : Temp.label) 
               : A.exp -> expty =
    let 

      (* Handles arithmetic, comparison, and equality cases. *)
      fun trexp (A.OpExp{left : A.exp,
                             oper : A.oper,
                             right : A.exp, 
                             pos : A.pos}) =
        let
          val {exp=left_exp,ty=tyleft} = trexp left
          val {exp=right_exp,ty=tyright} = trexp right
        in
          (print "OpExp\n";
          (case oper of 
            (A.PlusOp | A.MinusOp | 
            A.TimesOp | A.DivideOp) =>
              checkArithmetic(tyleft,tyright,pos)

          | (A.LtOp|  A.LeOp | 
            A.GtOp | A.GeOp) =>
            checkComparison(tyleft,tyright,pos)

          | (A.EqOp | A.NeqOp) =>
            checkEquality(tyleft,tyright,pos));

          (* returns either an arithmetic result or boolean (int) *)
          {exp=TR.opExp(left_exp,oper,right_exp),
           ty=TY.INT})
        end

        (* For ArrayExp, must:
        * - confirm that typ is in the symbol table;
        * - confirm that typ is array type;
        * - confirm that size is type int;
        * - confirm that init evaluates to same type as type of array. *)
        | trexp (A.ArrayExp{typ : S.symbol,
                                size : A.exp,
                                init : A.exp,
                                pos : A.pos}) =
              (case S.look(tenv,typ) of SOME t =>
                (case actual_ty(t,pos) of TY.ARRAY(array_t,unique) =>
                   let val {exp=size_exp,ty=size_t} = trexp size;
                       val {exp=init_exp,ty=init_ty} = trexp init;
                   in (print ("ArrayExp "
                        ^ "of type " ^ typeToStr array_t
                        ^ "\n");
                        checkSameType(array_t,init_ty,pos);
                        checkSameType(size_t,TY.INT,pos);
                       {exp=TR.arrayExp(size_exp,init_exp), 
                        ty=t})
                   end
                         | whatever => (error pos ("should be TY.ARRAY "
                          ^ " but is "^typeToStr(actual_ty(t,pos))^"\n");
                                        error_result))
                                    |  NONE => badLookup(typ,pos))

        (* Recursively handle a list of declarations.
        * Supplement the given venv and tenv.
        * Discard the new environments when done.*)
        | trexp (A.LetExp{decs, body, pos}) =
          let
            val {venv=venv',tenv=tenv',exp_list} = transDecs(venv,tenv,
                                                             level,break,
                                                             decs)
            (* For effiency, exps are prepended to the list as they are
            * translated, so we have to reverse the list before use. *)
            val exp_list = List.rev(exp_list)
            val {exp=body_exp,ty=ty} = transExp(venv',tenv',level,break) 
                                      body
          in
            {exp=TR.letExp(exp_list,body_exp),ty=ty}
          end

        (* To handle a sequence of expressions,
        * first translate each expressions to return a list
        * of {exp,type}. Then return the type of the last expression.*)
        | trexp (A.SeqExp(exp_list)) = 
          let
            val expanded = map (fn (exp,pos) => trexp exp) exp_list
            val translated_exps  = map (fn {exp,ty} => exp) expanded
            val translated_types = map (fn {exp,ty} => ty) expanded
            val last_ty = if List.null expanded then TY.UNIT (* empty *)
                            else List.last translated_types
          in
            {exp=TR.exp_sequence(translated_exps),ty=last_ty}
          end

        (* Confirms that the type is in the environment,
        * confirms that the fields match the type specified,
        * returns a fresh T.RECORD. *)
        | trexp (A.RecordExp{
              fields: (A.symbol * A.exp * A.pos) list,
              typ : A.symbol, pos : A.pos}) =

          let
            (* Does each field type exist,
            * and does each field exp match its type? 
            * Work through record_fields, matching each symbol
            * with the correct declared field, then confirm
            * that the declared exp evaluates to the record field type.
            *)
            fun checkFields((real_symbol,typ)::tail,fields) : unit = 
                (case 
                   (List.find
                    (fn (sym:A.symbol,exp:A.exp,pos:A.pos) => 
                      if sym <> real_symbol then false else true)
                      fields)
                  of 
                    NONE => (error pos ("declared field does not match"
                                       ^ " record definition");
                            checkFields(tail,fields))
                  | SOME (sym,exp,pos) => 
                      expIsType(venv,tenv,level,break,exp,typ,pos))
              | checkFields([],fields) = ()
          in
            (case S.look(tenv, typ) of
               NONE => badLookup(typ,pos)
             | SOME t =>
                 (* if so, is type a record type? *)
                 (case actual_ty (t,pos) of

                   TY.RECORD(record_fields,unique) =>
                   (* must evaluate field expressions for TR *)
                   let 
                     val field_a_exp_list = map (fn f => 
                                                  let val (sym,exp,pos) = f
                                                  in exp end) 
                                                fields

                     fun eval_field (field_a_exp : A.exp) 
                                     : TR.exp =

                       let val {exp=field_t_exp,ty=_} = trexp field_a_exp
                       in field_t_exp 
                       end
                     
                   in
                      (checkFields(record_fields,fields);
                       {exp=TR.recordExp(
                          map eval_field field_a_exp_list),
                        ty=TY.RECORD(record_fields,unique)})
                   end

                   | other_type => (error pos (S.name typ
                                             ^ " is not a record");
                                    error_result)))
          end


        (* Confirm that the expression matches the variable type. *)
        | trexp (A.AssignExp{var,exp,pos}) =
          let val {exp=var_exp,ty=var_ty} = trvar var
              val {exp=exp_exp,ty=exp_ty} = trexp exp
          in checkSameType(exp_ty,var_ty,pos);
             {exp=TR.assign(var_exp,exp_exp),ty=TY.UNIT}
          end

        (*
        * Confirm that the function is defined.
        * Confirm that the arguments are the correct type.
        *)
        | trexp (A.CallExp{func : A.symbol, 
                               args : A.exp list, 
                               pos : A.pos}) =
          (case S.look(venv, func) of
                SOME (Env.FunEntry{level = dec_level : TR.level,
                                   label : Temp.label,
                                   formals : Env.ty list,
                                   result : Env.ty}) =>
                  let
                    (* evaluate each argument to pass to TR.call *)
                    val arg_exps = map (fn (arg : A.exp) =>
                                          let val {exp=arg_exp,ty=_} = 
                                            trexp arg
                                          in arg_exp
                                          end)
                                       args
                  in
                  (* check formal types against their exp *)
                  (ListPair.appEq (* error if lists are not equal length *)
                    (fn (f : TY.ty, a : A.exp) => 
                         checkSameType(f, (#ty (trexp a)), pos))
                    (formals, args);
                  (* return the type associated with the function *)
                  print ("CallExp on "^S.name func^"\n");
                  {exp=TR.callExp(label,level,dec_level,arg_exps),
                   ty=result})
                  end

              (* symbol is not a function *)
              | SOME _ => (error pos (S.name func
                                       ^ " found in symbol table"
                                       ^ " but is not a function\n");
                            error_result)

              (* function not defined *)
              | NONE => badLookup(func,pos))

        (* Confirm that 'test' is an INT type,
        * that 'then' and 'else' type exist,
        * and also that they equal each other. *)
        | trexp (A.IfExp{test  : A.exp, 
                             then' : A.exp, 
                             else' : A.exp option, 
                             pos : A.pos}) =
          let val {exp=test_exp,ty=test_ty} = trexp test
              val {exp=then_exp,ty=then_ty} = trexp then'
          in
            checkSameType(test_ty,TY.INT,pos);
            case else' of SOME e =>                    (* else *)
              let val {exp=else_exp,ty=else_ty} = trexp e
              in checkSameType(then_ty,else_ty,pos);
                 {exp=TR.ifExp(test_exp,then_exp,SOME else_exp), 
                  ty=then_ty}
              end
                        | NONE =>                      (* no else *)
              {exp=TR.ifExp(test_exp,then_exp,NONE), ty=then_ty}
          end
        
        (* Test must be a bool (int), Body should be UNIT *)
        | trexp (A.WhileExp{test : A.exp, 
                                body : A.exp,
                                pos : A.pos}) =
          let
            val {exp=test_exp,ty=test_ty} = trexp test
            val done = Temp.newlabel() (* new place for loop to fall to *)
            val {exp=body_exp,ty=body_ty} = 
              (* translate the body, but with new 'done' label *)
              (transExp(venv,tenv,level,done) (body))
          in
            checkSameType(test_ty,TY.INT,pos);
            print "check body arg\n";
            checkSameType(body_ty,TY.UNIT,pos);
            {exp=TR.whileExp(test_exp,body_exp,done),
                                    ty=body_ty}
          end

        (* We handle this by rewriting it as a while loop, as per p. 166.
        * Appell warns about an edge case where limit = maxint;
        * we should eventually fix this. *)
        | trexp (A.ForExp{var : A.symbol,
                              escape : bool ref,
                              lo : A.exp,
                              hi : A.exp,
                              body : A.exp,
                              pos : A.pos}) =
          let
            val limit = S.symbol "limit"
            val limit_var = A.SimpleVar(limit, pos)
            val i_var = A.SimpleVar(var, pos)
            val decs = [A.VarDec{name = var,
                                     escape = escape,
                                     typ = NONE, (* Not sure why NONE. *)
                                     init = lo,
                                     pos = pos},
                        A.VarDec{name = limit,
                                     escape = ref false,
                                     typ = NONE, (* Not sure why NONE. *)
                                     init = hi,
                                     pos = pos}]
            val test = A.OpExp{
                          left = A.VarExp(i_var),
                          oper = A.LeOp,
                          right = A.VarExp(limit_var),
                          pos = pos}
            val while_body = A.SeqExp[
                              (body, pos),
                              (A.AssignExp{
                                         var = i_var,
                                         exp = A.OpExp{
                                            left = A.VarExp(limit_var),
                                            oper = A.PlusOp,
                                            right = A.IntExp(1),
                                            pos = pos},
                                         pos = pos}, pos)]
            val while_exp = A.WhileExp{test = test, 
                                            body = while_body,
                                            pos = pos}
            val let_exp = A.LetExp{decs = decs, 
                                       body = while_exp, 
                                       pos = pos}
          in
            trexp let_exp
          end
          
        | trexp (A.VarExp(var)) = trvar var
        | trexp (A.IntExp(k)) = {exp=TR.intExp(k),ty=TY.INT}
        | trexp (A.NilExp) = {exp=TR.const_zero,ty=TY.NIL}
        | trexp (A.StringExp(s,pos)) = 
          {exp=TR.stringExp(s),ty=TY.STRING}
        | trexp (A.BreakExp(pos)) = 
          {exp=TR.breakExp(break),ty=TY.UNIT}
        
      (* make sure to return only variables, not functions *)
      and trvar (A.SimpleVar(id,pos)) =
        (case S.look(venv,id)
          of SOME(Env.VarEntry{access=access,
                               ty=ty}) =>
                          (error pos 
                            ("good lookup simpleVar " ^ (S.name id));
            {exp=TR.simpleVar(access, level),
             ty=actual_ty (ty,pos)})
          |  SOME _ =>
            (error pos ("found function, not variable "
                        ^ S.name id);
            {exp=TR.const_zero, ty=TY.NIL})
          |  NONE => badLookup(id,pos))

        (* 
        * Confirms that the variable is a record type,
        * Then pulls out the record's fields and tries
        * to match them to the symbol, returning the type.
        *)
        | trvar (A.FieldVar(var, symbol, pos)) =
          let
            val {exp=trans_var,ty=var_ty} = trvar var
          in
            case var_ty of
                TY.RECORD(field_list,_) =>
                  (* field_list = (symbol * pos) list,
                  * match symbol to symbol to find field,
                  * return index to calculate offset *)
                  (case (List.find
                          (fn (sym,pos) => sym = symbol)
                          field_list) of
                    SOME field_t => 
                      (* now find the index of the field *)
                      let
                        (* get_ix returns 0 if field not found,
                        * which is dangerous,
                        * but field must be found by this point. *)
                        fun get_ix ((sym,pos)::tail, i) =
                          if sym = symbol then i
                            else get_ix (tail, i+1)
                          | get_ix (nil, i) = i
                      in
                        {exp=TR.fieldVar(trans_var,
                                                get_ix (field_list,0)),
                         ty=actual_ty (#2 field_t, pos)}
                      end
                  | NONE => (error pos "could not match field in record";
                            error_result))
              | other_type => (error pos (S.name symbol
                                         ^ " is not a record");
                                error_result)
          end

        (* Confirm that 'var' refers to an array,
        * and 'exp' evaluates to an int. *)
        | trvar (A.SubscriptVar(var : A.var, (* array variable *)
                                exp : A.exp, (* index *)
                                pos : A.pos)) =
          let
            val {exp=trans_var,ty=var_type} = trvar var
            and {exp=trans_exp,ty=exp_ty} = trexp exp
          in
            case var_type of
                TY.ARRAY(array_t,_) =>
                  (case exp_ty of
                       TY.INT =>
                          (print "subscriptVar\n";
                         {exp=TR.subscriptVar(trans_var,trans_exp),
                          ty=array_t})
                     | whatever => (error pos "subscript was not int";
                                    error_result))
              | whatever => (error pos ("var is " ^ (typeToStr whatever)
                                        ^ ", not array type");
                            error_result)
          end
    in
      trexp
    end

  (* Process the declarations one by one, adding them to the base env.
  * Collects the TR.exp produced by each transDec and returns 
  * them as a list. *)
  and transDecs (venv, tenv, level, break, dec_list) =
    (print "transDecs\n";
    foldl
      (fn (dec, {venv,tenv,exp_list}) => transDec(venv,tenv,exp_list,
                                                  level,break,dec))
        {venv=venv,tenv=tenv,exp_list=nil}
        dec_list)

  (* Confirms that the declared type matches the init type.
  * Allocates space for the variable, then creates a fragment to fill the
  * space with the init value. 
  * exp_list : While translating a Let Expression, transDecs accumulates a
  *   list of expressions; each call to transDec will add a new exp. *)
  and transDec (venv, tenv, exp_list, level, break,
                A.VarDec{name, typ : (A.symbol * A.pos) option,
                             init, escape : bool ref, pos}) =
    let
      val {exp=var_exp,ty} = transExp(venv,tenv,level,break) init
      val access = (TR.allocLocal level) (!escape)
    in
      print "semant VarDec\n";
      case typ of NONE => 
                   (* this case is a guess *)
                   {venv=S.enter(venv,name,
                                      Env.VarEntry{access=access,ty=ty}),
                    tenv=tenv,
                    exp_list=exp_list}
                | SOME (sym,pos) =>
                     (case S.look(tenv,sym) of
                          NONE => (error pos ("variable undefined :"
                                              ^ S.name sym);
                                              {venv=venv,tenv=tenv,
                                               exp_list=exp_list})
                        | SOME t => 
                          (checkSameType(ty,t,pos);
                          (* now commit to assigning everything *)
                          transVarDecHelper(access,level,var_exp,venv,name,
                                            ty,tenv,exp_list)))
    end

    (* Handles recursive definitions by doing a first pass,
    * in which every type is added as Type.NAME, an empty header.
    * Then a second pass calls transTy to fill in the definitions. *)
    | transDec (venv, tenv, exp_list, level, break, A.TypeDec(dec_list)) =
        let
          (* First, enter all declared symbols into table with
          * T.NAME(symbol_name, ref None).*)
          fun firstPassTypeDec(env,
            {name:A.symbol,ty:A.ty,pos:A.pos}::tail) =
              S.enter(env,name,TY.NAME(name,ref NONE))

            | firstPassTypeDec(env,[]) = env

            (* Now, fill in empty T.NAMEs with transTy.*)
          and secondPassTypeDec(env,
            {name:A.symbol,ty:A.ty,pos:A.pos}::tail) =
              (case S.look(env,name) of
                   SOME (TY.NAME(sym : S.symbol,
                                    r : TY.ty option ref)) =>
                                      (print ("typedec name "^S.name name
                                      ^" and ty " ^ typeToStr(transTy(env,ty)) ^
                                      "\n");
                                       r := SOME(transTy(env,ty));
                                       env)
                 | SOME _ => (error pos "typedef second pass failed"; 
                            env)
                 | NONE => (error pos "typedef second pass failed"; 
                            env))

            | secondPassTypeDec(env,[]) = env
            
          val tenv' = firstPassTypeDec(tenv,dec_list)
          val tenv'' = secondPassTypeDec(tenv',dec_list)
        in
          {venv=venv,tenv=tenv'',exp_list=exp_list}
        end

    (* Declare a function with a list of formals and a result type.
    * Confirm that result type exists.
    * Confirm that body expression checks with result type.
    * Confirm that param types exist.
    * Confirm no duplicate parameter names.
    * Finally, translation requires creating a new level,
    * turning the body into a Tree.stm, and storing a frag in TR.
    * A.FunctionDec({name: A.symbol,
                         params: A.field list,
                         result: (A.symbol * pos) option,
                         body: A.exp,
                         pos: A.pos} list)) =
    *)
    | transDec (venv, tenv, exp_list, level, break,
          A.FunctionDec(fn_dec_list : A.fundec list)) =

      let
        (* Returns an updated venv with new FunEntry *)
        fun addFunDecHeaders({name,params,result,body,pos},env)
          : Env.enventry S.table =
          let
            (* check function return py *)
            val ret_ty =
              case result of
                 NONE => TY.UNIT (* functions can return UNIT *)
               | SOME (sym,pos) =>
                   (case S.look(tenv,sym) of
                        SOME t => t
                      | NONE => (error pos ("could not find result type: "
                                      ^ S.name sym);
                                 TY.NIL))
            (* check function parameter list *)
            val formals =
              map
                (fn ({name,typ,pos,escape}: A.field) => 
                  case S.look(tenv,typ) of 
                       SOME t => t
                     | NONE => (error pos ("could not find param type: "
                                    ^ S.name typ);
                                TY.NIL))
                params

            val escape_list = map (fn {escape=esc,...} => !esc) params

            (* error if dupe found *)
            fun checkDuplicates ({name,typ,pos,escape}::tail) =
              (case (List.find
                    (fn (x) => x = name)
                    (map #name tail)) of
                     SOME x =>
                              (error pos ("field" ^ S.name name 
                                ^ "is duplicate"); checkDuplicates(tail))
                   | NONE => ())

              | checkDuplicates (nil) = ()
          in
            (print "addFunDecHeaders\n";
            checkDuplicates(params); (* no duplicate formal params *)
            (* Note that _this_ is where the newLevel is called *)
            S.enter(env,name,Env.FunEntry(
                                  {label=Temp.newlabel(),
                                   level=TR.newLevel{
                                     parent=level,
                                     name=name,
                                     formals=escape_list},
                                   formals=formals,result=ret_ty})))
          end

      in

        let
          val venv' = foldl addFunDecHeaders venv fn_dec_list

          (* Add VarEntries to venv', creating venv''; allocate locals *)
          fun transSingleDec ({name,params,result,body,pos},venv') = 
            let 
              (* pull the new entry for this particular fn *)
              val SOME(Env.FunEntry{level=this_level,
                                    result,...}) = S.look(venv',name)

              val access_l = TR.formals this_level (* access list *)

              fun addParam({name,escape,typ,pos}, access, env) 
                  : Env.enventry S.table =
                case S.look(tenv,typ) of SOME t => 
                                         S.enter(env,name,
                                         Env.VarEntry{access=access, ty=t})
                                            | NONE => 
                                      (error pos ("could not find param : "
                                                  ^ S.name typ);
                                                  env)

              val venv'' = ListPair.foldl addParam venv' (params,access_l)

              val {exp=body_exp,ty=body_ty} = transExp(venv'',tenv,
                                                this_level,break)
                                       body
            in
              (print ("trans function dec " ^ S.name name ^ "\n");
              checkSameType(body_ty,result,pos);
              (* procEntryExit creates a code fragment in TR *)
              TR.procEntryExit({level=this_level,body=body_exp}))
            end
        in
          print "trans FunDecList\n";
          app (fn (fndec) => transSingleDec(fndec,venv')) fn_dec_list;
          (* return venv', not venv''; we're popping out *)
          {venv=venv',tenv=tenv,exp_list=exp_list} 
        end
      end

  (* After a successful lookup, actually allocates and fills space. *)
  and transVarDecHelper(access,level,var_exp,venv,name,ty,tenv,exp_list) =
    let val var_location = TR.simpleVar(access, level)
        val init_exp = TR.assign(var_location, var_exp)
    in
      {venv=S.enter(venv,name,
                         Env.VarEntry{access=access, ty=ty}),
        tenv=tenv,
        exp_list=init_exp::exp_list}
    end

     (* For a Name Type, just look up the symbol in the symbol table. *)
  and transTy (tenv, A.NameTy(symbol,pos)) =
    (case S.look(tenv,symbol) of 
         SOME t => t
       | NONE => 
            (error pos ("undefined variable: "
                        ^ S.name symbol);
            TY.NIL))

    (* From a Record Type of field list,
    * where field = {name, escape, typ, pos},
    * create a TY.RECORD of (symbol * ty) list * unique. *)
    | transTy (tenv, A.RecordTy(fields)) =
      TY.RECORD (
        map
          (fn {name,escape,typ,pos} =>
            case S.look(tenv,typ) of
                SOME t => (name,t)
              | NONE   => (error pos ("undefined var: "
                                      ^ S.name name);
                          (name,TY.NIL)))
            fields,
         ref())

      (* Array TY work like Name TY. *)
    | transTy (tenv, A.ArrayTy(symbol, pos)) =
      (case S.look(tenv,symbol) of
        SOME t => (print ("transTY ArrayTy name "^(S.name symbol)
                          ^" of type "^(typeToStr(t))^"\n");
                          TY.ARRAY(t,ref ()))
      | NONE => 
            (error pos ("undefined variable: "
                        ^ S.name symbol);
            TY.NIL))

  fun transProg (tree : A.exp) : unit =
    let
      (* As SML compiles the compiler, it runs some code, whose side
      * effects fill up our local variables, and cause trouble down the
      * line. So, we gotta flush the buffer. 
      * I wouldn't have caught this one on my own. *)
      val _ = TR.reset_frags() 
      val _ = Temp.reset_counters() 

      (* start transExp with basic values of everything *)
      val main_level = TR.newLevel{parent=TR.outermost,
                                          formals=[],name=Temp.newlabel()}
      val done_label = Temp.newlabel() (* where to go if break *)
      val {exp,ty} = transExp(Env.base_venv, 
                              Env.base_tenv,
                              main_level,
                              done_label) tree
    in
      TR.procEntryExit{level=main_level,body=exp}
    end

end
