signature MAKEGRAPH =
sig    
  val instrs2graph : Assem.instr list -> 
                       Flow.flowgraph * Graph.node list
end

structure MakeGraph : MAKEGRAPH =
struct
  fun instrs2graph (assems : Assem.instr list) =
  let val g = Graph.newGraph()

      (* first pass -- add sequential edges *)
      val {g,nodes} = foldl makeControlFlowGraph 
                                {g=g,nodes=nil}
                                assems
      (* nodes were added to list in rev order *)
      val nodes = List.rev nodes 
      (* second pass -- add jump edges *)
      val {g,nodes,assems,from_ix} = foldl addJumpsToFlowGraph
                                                 {g=g,nodes=nodes,
                                                  assems=assems,
                                                  from_ix=0}
                                                  assems
      val {def_tab,use_tab} = makeDefandUseSets(assems, nodes)
      val ismove = makeMoveSet(assems, nodes)

  in (Flow.FGRAPH{control=g,def=def_tab,use=use_tab,ismove=ismove},
      nodes)
  end

  (* Produce a boolean for each instr, add to table. *)
  and makeMoveSet(assems : Assem.instr list, 
                        nodes : Graph.node list) =
        foldl (fn ((assem,node), tab) => 
                case assem of Assem.MOVE{...} => 
                      Graph.Table.enter(tab,node,true)
                                      | _ => tab)
              Graph.Table.empty
              (ListPair.zip(assems,nodes))

  and makeDefandUseSets(assems : Assem.instr list, 
                        nodes : Graph.node list) 
                           : {def_tab : Temp.temp list Graph.Table.table, 
                              use_tab: Temp.temp list Graph.Table.table} =

  let fun makeDefandUseSets'((instr,node), {def_tab, use_tab}) =
            case instr of 
                  Assem.OPER{dst:Assem.temp list,
                             src:Assem.temp list,assem,jump} =>
                     {def_tab=Graph.Table.enter(def_tab,node,dst),
                      use_tab=Graph.Table.enter(use_tab,node,src)}

                | Assem.MOVE{dst:Assem.temp,
                             src:Assem.temp,assem} =>
                     {def_tab=Graph.Table.enter(def_tab,node,[dst]),
                      use_tab=Graph.Table.enter(use_tab,node,[src])}

                | Assem.LABEL{...} => {def_tab=def_tab,use_tab=use_tab}
  in
    foldl makeDefandUseSets' 
          {def_tab=Graph.Table.empty,
           use_tab=Graph.Table.empty}
          (ListPair.zip(assems,nodes))
  end

  (* For each instruction, make a new node in g,
  * then make an edge between g and the most recent node in the list. *)
  and makeControlFlowGraph (instr : Assem.instr, 
                            {g : Graph.graph, 
                             nodes: Graph.node list}) = 
  let val k = Graph.newNode(g)
  in (case nodes of 
           j :: tail =>
                Graph.mk_edge{from=j,to=k} (* succ(j) U k, pred(k) U j *)
         | nil => () );
     {g=g,nodes=(k::nodes)}
  end

  (* When we find a OPER instruction with JUMPs,
  * match the label in the jump to its instruction in the list,
  * then take the index of the instruction and find the matching node,
  * finally making an edge from the current instruction to the match. *)
  and addJumpsToFlowGraph(instr : Assem.instr, 
                          {g : Graph.graph, 
                           assems : Assem.instr list,
                           nodes : Graph.node list,
                           from_ix : int}) =

    case instr of Assem.OPER{assem,dst,src,jump} =>

      (case jump of SOME(label_list) =>
        let val g' = matchLabelsAndAddEdges(label_list,g,
                                           assems,nodes,from_ix)
        in {g=g',assems=assems,nodes=nodes,
            from_ix=from_ix+1}
        end
         | NONE =>
             {g=g,assems=assems,nodes=nodes,
              from_ix=from_ix+1})

       | _ => {g=g,assems=assems,nodes=nodes,
               from_ix=from_ix+1}

  (* Looks for jump statements, and adds edges to g for each edge. *)
  and matchLabelsAndAddEdges(label :: tail, g, assems, 
                             nodes, from_ix) =

  let val to_ix = getIx(label, assems, 0)
  in Graph.mk_edge{from=List.nth(nodes,from_ix),
                   to=List.nth(nodes,to_ix)};
     matchLabelsAndAddEdges(tail, g, assems, nodes, from_ix)
  end
  
    | matchLabelsAndAddEdges (nil, g, assems, nodes, from_ix) = g

  (* Return the index of the instruction to which this label refers. *)
  and getIx(to_label, instr :: tail, counter) : int = 
        (case instr of Assem.LABEL{assem,lab} =>
          if (to_label = lab) then counter 
          else getIx(to_label, tail, counter+1)
        | _ => getIx(to_label, tail, counter+1))
    | getIx(label, nil, counter) = (print "error getIx\n"; counter) 

end
