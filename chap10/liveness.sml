signature LIVENESS =
sig 
  datatype igraph =
    IGRAPH of {graph: Graph.graph,
               tnode: Temp.temp -> Graph.node,
               gtemp: Graph.node -> Temp.temp,
               moves: (Graph.node * Graph.node) list}

  val interferenceGraph :
        (Flow.flowgraph * Graph.node list) ->
          igraph * (Graph.node -> Temp.temp list)

end

structure Liveness : LIVENESS =
struct
  datatype igraph =
    IGRAPH of {graph: Graph.graph,
               tnode: Temp.temp -> Graph.node,
               gtemp: Graph.node -> Temp.temp,
               moves: (Graph.node * Graph.node) list}

  type liveSet = unit Temp.Table.table * Temp.temp list
  type liveMap = liveSet Graph.Table.table

  (* Begin by creating a liveSet, and then a liveMap, for the given
  * flow control graph.
  * Then create the interference graph by creating edges (d,t) for
  * each defined temp d and all live_out temps t at the defining node. *)
  fun interferenceGraph (fg as Flow.FGRAPH{control,def,use,ismove},
                         nodes : Graph.node list) =
  let val fake_node = Graph.newNode(control) 
      fun fake_tnode (t) = fake_node
      fun fake_gtemp (n) = Temp.newtemp()
      val fake_moves = nil
    val live_map = makeLiveMap(fg,nodes)
  in
    (IGRAPH{graph=control,tnode=fake_tnode,gtemp=fake_gtemp,moves=fake_moves},
      (fn n : Graph.node => nil))
  end

  (* A liveMap is a mapping between nodes and their live-out sets.
  * We must work backward through the node list. For every 'use' variable
  * we counter, we must then search backward through the node list,
  * marking each node's live-out set with the given variable as we go,
  * until we hit the 'def' node for that variable. Then we move on to
  * the next variable, for a total of O(nodes * temps) loops. *)
  and makeLiveMap (fg : Flow.flowgraph, 
                   nodes : Graph.node list) : liveMap =
   let val nodes = List.rev(nodes) (* information flows backward *)
       (* initialize a new liveSet for each node *)
       val map = initLiveMap{map=Graph.Table.empty,fg=fg,nodes=nodes}
       (* fill out the liveSets in reverse order *)
       val {map=map,nodes=nodes,fg=_} = foldl fillLiveMap
                                              {map=map,nodes=nodes,fg=fg}
                                              nodes
   in map
   end

 (* create a liveset table and liveset list for each node *)
  and initLiveMap {map : liveMap, 
                   fg: Flow.flowgraph, 
                   nodes: Graph.node list} : liveMap = 
        foldl (fn (node, map) => 
                    Graph.Table.enter(map,
                                      node,
                                      (Temp.Table.empty, nil)))
              map
              nodes

  and fillLiveMap (node : Graph.node,
                   {map : liveMap,
                    nodes : Graph.node list,
                    fg as Flow.FGRAPH{control,def,use,ismove}}) =
  let val map' = case Graph.Table.look(use,node) of SOME use_temps =>
                    (* add each use_temp to this node's live set *)
                    (*let val map = addTempsToNodeLiveSet(use_temps,map,node)*)
                    (* run through the nodes to add to more live sets *)
                    let val {map,...} = foldl fillLiveMapBackward
                                    {map=map,node=node,nodes=nodes,fg=fg}
                                    use_temps
                    in map end
                | NONE => map

  in {fg=fg,nodes=nodes,map=map'} end

  (* Take a list of temps and a given node,
  * add them all to the live set in liveMap[node]. *)
  and addTempsToNodeLiveSet(use_temps : Temp.temp list, 
                            map : liveMap,
                            node : Graph.node) =
        case Graph.Table.look(map, node) of
             SOME (live_table, live_list) => 
                  let val live_table = 
                      (* We add the temp to the table with value (),
                      * because this table is just to check membership *)
                      foldl (fn (t, tab) => Temp.Table.enter(tab,t,()))
                            live_table
                            use_temps
                      (* also add to list if not already present *)
                      val live_list = foldl addTempToList 
                                            live_list 
                                            use_temps
                  in Graph.Table.enter(map, node, (live_table, live_list))
                  end

           | NONE => (print "error: Liveness.addTemps...\n"; map)

  (* return list unchanged if temp not present, otherwise add *)
  and addTempToList (t : Temp.temp, 
                     live : Temp.temp list) : Temp.temp list =
        case (List.find (fn live_temp => t = live_temp) live) of
             SOME _ => live
           | NONE => t :: live

  (* Trace this temp backward through the graph, using the 'pred'
  * feature of the flow graph. Search through the preceding nodes
  * using depth-first search until we find the defining node for this
  * temp. At each node, add the temp to the live-out for that node. *)
  and fillLiveMapBackward(t : Temp.temp, {map : liveMap,
                                          node : Graph.node,
                                          nodes : Graph.node list,
                                          fg as Flow.FGRAPH{control,
                                                            def,use,
                                                            ismove}}) = 
  let val map = addTempsToNodeLiveSet([t],map,node)
  in if tempDefinedInNode(t,def,node) then {map=map,node=node,
                                            nodes=nodes,fg=fg}
     else let val pred_node = findPred(node,nodes)
          in fillLiveMapBackward(t, {map=map,node=pred_node,
                                     nodes=nodes,fg=fg})
          end
  end

  (* Use depth-first search to find the next predecessor node to search.
  * Prevent cycles by storing a list of searched node indices from the
  * master node list. *)
  and findPred (node : Graph.node, 
                all_nodes : Graph.node list) : Graph.node =
  
  hd (Graph.pred(node))

  (* true if the node has a def list, and if this temp is in that list. *)
  and tempDefinedInNode(t : Temp.temp, 
                        def : Temp.temp list Graph.Table.table,
                        node : Graph.node) : bool =
        case Graph.Table.look(def,node) of SOME def_list =>
          (case List.find (fn x => x = t) 
                          def_list of SOME _ => true
                                    | NONE => false)
        | NONE => false

end
