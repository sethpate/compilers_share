signature FRAME =
(* Taken from Appell, p 134 *)

sig
  type frame
  type access

  datatype frag = PROC of {body : Tree.stm, frame : frame}
		| STRING of Temp.label * string

  val newFrame : {name : Temp.label,
  		  formals : bool list} -> frame
  val name : frame -> Temp.label
  val string : Temp.label * string -> string
  val formals : frame -> access list
  val allocLocal : frame -> bool -> access
  val procEntryExit1 : frame * Tree.stm -> Tree.stm
  val procEntryExit2 : frame * Assem.instr list -> Assem.instr list
  val procEntryExit3 : frame * Assem.instr list -> {prolog:string,
  						   body:Assem.instr list,
						   epilog:string}
  val wordSize : int
  val exp : access -> Tree.exp -> Tree.exp
  val externalCall : string * Tree.exp list -> Tree.exp

  type register = string
  val tempMap : register Temp.Table.table

  (* here be registers *)
  val FP : Temp.temp (* frame pointer *)
  val SP : Temp.temp (* stack pointer *)
  val RV : Temp.temp (* return value (as seen by callee) *)
  val RA : Temp.temp (* return address (as seen by callee?) *)
  val ZERO : Temp.temp (* zero register *)

  val specialregs : Temp.temp list
  val argregs : Temp.temp list
  val callersaves : Temp.temp list
  val calleesaves : Temp.temp list

end
