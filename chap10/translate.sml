(* Seth looked at an implementation at github.com/sunchao/tiger *)

signature TRANSLATE =
sig
  type level
  type access (* different from Frame.access *)

  datatype exp = Ex of Tree.exp (* 'expression' *)
               | Nx of Tree.stm (* 'no result' *)
               (* conditional *)
               | Cx of Temp.label * Temp.label -> Tree.stm 

  val outermost : level
  val newLevel : {parent: level, name: Temp.label,
                  formals: bool list} -> level
  val formals : level -> access list
  val allocLocal : level -> bool -> access
  val procEntryExit : {level : level, body : exp} -> unit

  structure Frame : FRAME
  val getResult : unit -> Frame.frag list
  val fragments : Frame.frag list ref (* global stash for frags *)
  val reset_frags : unit -> unit

  val simpleVar : access * level -> exp
  val subscriptVar : exp * exp -> exp
  val fieldVar : exp * int -> exp
  val assign : exp * exp -> exp

  val exp_sequence : exp list -> exp
  val letExp : exp list * exp -> exp
  val stringExp : string -> exp
  val intExp : int -> exp
  val arrayExp : exp * exp -> exp
  val recordExp : exp list -> exp
  val opExp : exp * Absyn.oper * exp -> exp 
  val breakExp : Temp.label -> exp
  val whileExp : exp * exp * Temp.label -> exp
  val callExp : Temp.label * level * level * exp list -> exp
  val ifExp : exp * exp * exp option -> exp

  val const_zero : exp

end

structure Translate : TRANSLATE =
struct

  (* Choose an implementation of frame; in this case, MIPS *)
  structure Frame : FRAME = MipsFrame

  structure T : TREE = Tree 

  (* A 'level' may either be a frame and its parent level,
  * or the original 'outer' level for the main program. *)
  datatype level = OuterLevel of unit ref
                 | InnerLevel of ({parent : level, frame : Frame.frame} 
                                   * unit ref) (* unique id *)

  (* basic environment for the whole program.
  * This is the level that 'library' functions are declared in.
  * 'Does not contain a frame or formal parameter list' p. 143. *)
  val outermost = OuterLevel(ref ())

  (* global list of fragments; we add with add_frag_to_globals *)
  val fragments : Frame.frag list ref = ref nil 

  (* flush fragments buffer *)
  fun reset_frags () = fragments := nil

  (* prepend (more efficient) as a side effect (with refs and stuff) *)
  fun add_frag_to_globals (f : Frame.frag) : unit =
    fragments := f::(!fragments)

  datatype exp = Ex of T.exp (* 'expression' *)
               | Nx of T.stm (* 'no result' *)
               (* conditional *)
               | Cx of Temp.label * Temp.label -> T.stm 

  (* useful for debugging; returns for NilExp *)
  val const_zero = Ex (T.CONST(0))

  (* convenient for turning a list of T.stm into a single T.stm,
  * using the definition T.SEQ = stm * stm. *)
  fun seq [stm : T.stm] : T.stm = stm
    | seq [stm_1, stm_2] = T.SEQ(stm_1, stm_2)
    | seq (stm::tail) = T.SEQ(stm, seq(tail))

  (* Dark magic contained here; not well understood by mortals; p.153 *)
  fun unEx (Ex e) : T.exp = e (* simple enough *)
    (* condtm jumps to label t if true, f if false;
    * therefore this returns 1 or 0 according to condstm *)
    | unEx (Cx condstm) : T.exp =
      let val r = Temp.newtemp() (* result *)
          val t = Temp.newlabel() (* code to run if true *)
          and f = Temp.newlabel() (* code to run if false *)
      in T.ESEQ(seq[T.MOVE(T.TEMP r, T.CONST 1),
                       condstm(t,f),
                       T.LABEL f,
                       T.MOVE(T.TEMP r, T.CONST 0),
                       T.LABEL t],
                   T.TEMP r)
      end
    (* evaluate statement s, then return nil *)
    | unEx (Nx s) = T.ESEQ(s, T.CONST 0)

  (* Given any exp, converts it to a statement form. *)
  fun unNx (Nx s) : T.stm = s
    | unNx (Ex e) = T.EXP(e) (* evaluate e, discard result *)
    | unNx (Cx condstm) = 
      let val r = Temp.newtemp() (* result *)
          val t = Temp.newlabel() (* code to run if true *)
          and f = Temp.newlabel() (* code to run if false *)
      in seq[T.MOVE(T.TEMP r, T.CONST 1),
                           condstm(t,f),
                           T.LABEL f,
                           T.MOVE(T.TEMP r, T.CONST 0),
                           T.LABEL t]
      end

  (* Given any exp, converts it to a conditional. *)
  fun unCx (Cx condstm) : Temp.label * Temp.label -> T.stm = condstm
    (* Quicker to skip the conditional and just jump to t/f. 
    * Remember that JUMP needs a list of all the labels it _could_ go,
    * because of the halting problem, I think. *)
    | unCx (Ex(T.CONST 1)) = (fn (t,f) => T.JUMP(T.NAME t, [t]))
    | unCx (Ex(T.CONST 0)) = (fn (t,f) => T.JUMP(T.NAME f, [f]))
    (* I think testing for zero is faster than testing for 1? *)
    | unCx (Ex e) = (fn (t,f) => T.CJUMP(T.NE,e,T.CONST 0,t,f))
    (* Leaving out unCx (Nx _), as this last case "should not appear 
    * in a well typed Tiger program." p.154. *)

  (* Pretty hazy here but it seems like we're usually turning this
  * into an ESEQ, where all but the last term are converted into a 
  * seq of stms. If the last element is Nx, though, I guess we just
  * return a seq of stms, not an ESEQ. edge cases when len is 0 or 1. *)
  fun exp_sequence (exp_l : exp list) : exp =
  let val len = List.length exp_l
  in
    if len = 0 then Nx(T.EXP(T.CONST 0))
    else if len = 1 then hd(exp_l)
         else let val all_but_last = seq(map unNx (List.take(exp_l,len-1)))
                  val last = List.last(exp_l)
              in case last of Nx(s) => Nx(T.SEQ(all_but_last,s))
                            | _     => Ex(T.ESEQ(all_but_last,unEx(last)))
              end
  end


  (* Different from the Frame.access. Is this where view shift is done? *)
  type access = level * Frame.access

  (* Stores a 'fragment' of straight-line (I think) code for a function,
  * in the local 'fragments' list. By calling procEntryExit1, it also
  * performs view shift, I think? So you have to pass the body expression
  * through procEntryExit1 to make it work. *)
  fun procEntryExit {level : level, body : exp} : unit = 
    case level of 
      InnerLevel({parent=_,frame},_) =>
        (* A little weird here -- when we translate the LetExp, its body
        * comes back as a T.exp; a value. However, when declaring the
        * function, the body is a T.stm. I'm not sure why yet. *)
        let val body_stm = Frame.procEntryExit1(frame,unNx body)
        in add_frag_to_globals(Frame.PROC{frame=frame,
                                           body=body_stm})
        end
    | OuterLevel(id) => ()

  (* Needs to be written *)
  (* 'getResult can be used to extract the (local) fragment list p.170 *)
  fun getResult () = !fragments

  (* Returns a 'false' bool for each item in list. 
  * False because variables are first assumed not to escape. *)
  fun bools (formals) : bool list =
    map (fn (x) => false) formals

  (* Calls newFrame, passing on the name and the list of escape bools.
  * To support static links, adds a boolean escape 'true' to the
  * beginning of the 'formals' argument; this is where the static link
  * will be saved in the new frame. *)
  fun newLevel {parent : level,
                name : Temp.label,
                formals : bool list} : level =
    InnerLevel ({parent=parent,
                  frame=Frame.newFrame{name=name, 
                                       formals=true::formals}},
                ref () ) (* last bit is unique id to tell levs apart *)

  (* Calls allocLocal, passing along the frame from level. *)
  fun allocLocal (l : level) : bool -> access =
    case l of InnerLevel ({parent=_,frame=f}, ident) =>
      fn (b: bool) => (l, (Frame.allocLocal f) (b))

  (* Retrieves an 'access list' representing the location for
  * each formal parameter, as a Translate.access.
  * Because the first parameter is the static link,
  * only returns the list after the first parameter. *)
  fun formals (l : level) : access list =
    case l of InnerLevel ({parent=_,frame=f}, ident) =>
      let val (static_link::formals) = Frame.formals f
        (* Turn into Translate.access from MipsFrame.access *)
      in (map (fn x => (l,x)) formals)
      end
       | OuterLevel(ident) => []

  fun levelID(InnerLevel(_,id)) : unit ref = id
    | levelID(OuterLevel id) = id

  (* Retrieve the static link for the dec_level.
  * Should be called initially with T.TEMP(Frame.FP) *)
  fun staticLink (sl : T.exp, 
                   call_level : level, 
                   dec_level : level) : T.exp =
    let
      val dec_id = levelID(dec_level)
      and call_id = levelID(call_level)
    in
      if call_id = dec_id then sl
      (* case statements here are out of control, i know *)
      else case dec_level of InnerLevel({parent=_,frame=dec_f}, dec_id) =>
        (case call_level of 
             InnerLevel({parent=parent,frame=call_f}, call_id) =>
          let val parent_fp = Frame.exp (hd(Frame.formals call_f)) (sl)
          in staticLink (parent_fp, parent, dec_level)
             end
           | OuterLevel id => sl)
         | OuterLevel id => sl
    end


  (* The first 'access' parameter indicates the level and frame
  * of declaration. The second 'level' parameter is the calling frame. *)
  fun simpleVar ((dec_level : level, dec_acc : Frame.access) : access, 
                  call_level : level) : exp =
    (* Peel back static links until we find the declaration level.
    * Once we find the declaration level, return its Frame.access. *)
    let val sl = staticLink(T.TEMP(Frame.FP),
                            call_level,
                            dec_level)
    in
      Ex(Frame.exp (dec_acc) (sl))
    end

  (* should be simple -- just find the offset from the pointer *)
  fun subscriptVar (var_pointer : exp, index : exp) =
    Ex(T.MEM(T.BINOP(T.PLUS,
                           unEx var_pointer,
                           T.BINOP(T.MUL,
                                      unEx index,
                                      T.CONST(Frame.wordSize)))))

  (* very similar to subscript var, except that the index is an int *)
  fun fieldVar (var_pointer : exp, index : int) =
    Ex(T.MEM(T.BINOP(T.PLUS,
                           unEx var_pointer,
                           T.BINOP(T.MUL,
                                      T.CONST(index),
                                      T.CONST(Frame.wordSize)))))

  (* Absyn.OpExps can be T.BINOPs or ESEQ (with T.CJUMP) *)
  fun opExp (left : exp, oper : Absyn.oper, right : exp) : exp =
    let fun arithmetic (left,oper,right) : T.exp =
          let val arith_op = case oper of
                                 Absyn.PlusOp => T.PLUS
                               | Absyn.MinusOp => T.MINUS
                               | Absyn.DivideOp => T.DIV
                               | Absyn.TimesOp => T.MUL
          in
            T.BINOP(arith_op,unEx left,unEx right)
          end

        fun comparison (left,oper,right) : exp =
          let val comp_op = case oper of
                                 Absyn.GtOp => T.GT
                               | Absyn.GeOp => T.GE
                               | Absyn.LtOp => T.LT
                               | Absyn.LeOp => T.LE
                               | Absyn.EqOp => T.EQ
                               | Absyn.NeqOp => T.NE
          in
            Cx(fn (t,f) => T.CJUMP(comp_op,
                                   unEx left, unEx right,
                                   t, f))
          end

    in
      case oper of
           (Absyn.PlusOp | Absyn.MinusOp | 
            Absyn.DivideOp | Absyn.TimesOp) => 
              Ex(arithmetic(left,oper,right))

         | (Absyn.GtOp | Absyn.GeOp | Absyn.LtOp | Absyn.LeOp |
            Absyn.EqOp | Absyn.NeqOp) => 
              comparison(left,oper,right)
    end

  (* The result of breaking in a loop. Simple jumps to 'done'. *)
  fun breakExp (done : Temp.label) : exp =
    Nx(T.JUMP(T.NAME(done), [done]))

  (* The loop starts at the 'test' label, and runs the test with CJUMP.
  * If the text expression is true (=1), jump to body, then back to test.
  * Otherwise, jump to the 'done' label. *)
  fun whileExp (test : exp, body : exp, done_lab : Temp.label) : exp =
  let val test_lab = Temp.newlabel()
      val body_lab = Temp.newlabel()
  in
    Nx(seq[T.LABEL(test_lab), (* sets bookmark for test code *)
           (* NEQ 0 is a little awkward but it's faster than EQ 1? *)
           T.CJUMP(T.NE, T.CONST(0), unEx test, 
                      body_lab, done_lab),
           T.LABEL(body_lab), (* again, bookmarks the body *)
           unNx body, (* have to actually run the body here *)
           (* because we bookmarked test_lab, we can jump to it *)
           T.JUMP(T.NAME(test_lab), [test_lab]),
           T.LABEL(done_lab)])
  end

  (* Must look up the static link for the dec_level and pass as first arg;
  * this has a role in making the whole static link apparatus work? *)
      (* Tiger built-ins are declared at 'Outermost' level *)
  fun callExp (label : Temp.label,
               OuterLevel id,
               call_level : level,
               args : exp list) : exp =
        Ex(T.CALL(T.NAME(label), (map unEx args)))

    | callExp (label : Temp.label, 
               dec_level : level, 
               call_level : level,
               args : exp list) : exp =
   let
     val sl = staticLink(T.TEMP(Frame.FP), call_level, dec_level)
     val arg_exps = map unEx args
   in
     Ex(T.CALL(T.NAME(label),sl::arg_exps))
   end

  (* Run all the declarations (statements), then run the body (exp). *)
  fun letExp (declarations : exp list, body : exp) : exp =
    if List.length(declarations) = 0 then body
      else Ex(T.ESEQ(seq(map unNx declarations),
                        unEx body))
           
  (* stupid (but correct?) version.
  * Note that "else" can be NONE, in which case...
  *      we'll think of something. *)
  fun ifExp (if_term: exp, then_term: exp, else_term: exp option) : exp =
  let val t = Temp.newlabel() and f = Temp.newlabel() 
                              and join = Temp.newlabel()
      val r = Temp.newtemp()
      val then_exp = unEx then_term
      val else_exp = case else_term of NONE => Tree.CONST(0)
                                     | SOME e => unEx e
  in Ex(Tree.ESEQ(seq[unCx if_term (t,f),
                      Tree.LABEL(t),
                      Tree.MOVE(Tree.TEMP r, then_exp),
                      Tree.JUMP(Tree.NAME(join),[join]),
                      Tree.LABEL(f),
                      Tree.MOVE(Tree.TEMP r, else_exp),
                      Tree.JUMP(Tree.NAME(join),[join]),
                      Tree.LABEL(join)],
                  Tree.TEMP(r)))
  end

  (* needs to look up existing strings and use them if they're there *)
  fun stringExp (s : string) : exp =
    let val lab = Temp.newlabel()
        val frag = Frame.STRING(lab,s) 
    in
      print "Translate.stringExp needs lookup implemented\n";
      add_frag_to_globals(frag);
      Ex(T.NAME(lab)) 
    end

  fun intExp (k : int) : exp = Ex(T.CONST(k))

  (* Note that you have to call unEx on the args first. *)
  fun arrayExp (size : exp, init_value : exp ) : exp =
    Ex(Frame.externalCall("initArray", [unEx(size), unEx(init_value)]))
     
  (* allocate n*W bytes on the heap in temp r,
  * then initialize offsets from temp r, one per field in record. *)
  fun recordExp (fields : exp list) : exp =
    let val r = Temp.newtemp()
        fun init_seq_list (e::tail : exp list,
                           n : int) : T.stm list =
          T.MOVE(T.MEM(T.BINOP(T.PLUS,
                                        T.TEMP r,
                                        T.CONST(n * Frame.wordSize))),
                    unEx e) :: init_seq_list (tail, n + 1)
          | init_seq_list (nil, n) = nil
    in
      Ex(T.ESEQ(T.SEQ(T.MOVE(T.TEMP r,
                                     Frame.externalCall(
                                        "malloc",
                                        [T.CONST(Frame.wordSize *
                                                  length(fields))])),
                           seq(init_seq_list(fields,0))),
                  T.TEMP(r)))
    end

    (* After space for a variable has been allocated at "target",
    * returns an expression to store "value" in that space. *)
  fun assign(target : exp, value : exp) : exp =
    Nx(T.MOVE(unEx target, unEx value))

  (* needs to look up existing strings and use them if they're there *)
  fun stringExp (s : string) : exp =
    let val lab = Temp.newlabel()
        val frag = Frame.STRING(lab,s) 
    in
      print "Translate.stringExp needs lookup implemented\n";
      add_frag_to_globals(frag);
      Ex(T.NAME(lab)) 
    end

  fun intExp (k : int) : exp = Ex(T.CONST(k))

  (* Note that you have to call unEx on the args first. *)
  fun arrayExp (size : exp, init_value : exp ) : exp =
    Ex(Frame.externalCall("initArray", [unEx(size), unEx(init_value)]))
     
  (* allocate n*W bytes on the heap in temp r,
  * then initialize offsets from temp r, one per field in record. *)
  fun recordExp (fields : exp list) : exp =
    let val r = Temp.newtemp()
        fun init_seq_list (e::tail : exp list,
                           n : int) : T.stm list =
          T.MOVE(T.MEM(T.BINOP(T.PLUS,
                                        T.TEMP r,
                                        T.CONST(n * Frame.wordSize))),
                    unEx e) :: init_seq_list (tail, n + 1)
          | init_seq_list (nil, n) = nil
    in
      Ex(T.ESEQ(T.SEQ(T.MOVE(T.TEMP r,
                                     Frame.externalCall(
                                        "malloc",
                                        [T.CONST(Frame.wordSize *
                                                  length(fields))])),
                           seq(init_seq_list(fields,0))),
                  T.TEMP(r)))
    end

    (* After space for a variable has been allocated at "target",
    * returns an expression to store "value" in that space. *)
  fun assign(target : exp, value : exp) : exp =
    Nx(T.MOVE(unEx target, unEx value))

end
