CM.make "sources.cm";

structure Tr = Translate
structure F : FRAME = MipsFrame
(*structure R = RegAlloc*)

(* after allocation, will search table of temp names *)
val tempname = Temp.makestring

(* helpful formatting function that i stole shamelessly. *)
fun addtab instrs =
  map (fn (i) => case i of l as Assem.LABEL _ => l
                              | Assem.OPER{assem,src,dst,jump} =>
              Assem.OPER{assem="\t"^assem,src=src,dst=dst,jump=jump}
                              | Assem.MOVE{assem,dst,src} =>
              Assem.MOVE{assem="\t"^assem,src=src,dst=dst})
      instrs

fun emitproc out (F.PROC{body,frame}) =
  let val _ = print ("\nemit " ^ Symbol.name(F.name frame) ^ "\n\n")
      val _ = Printtree.printtree(out,body)
      val _ = print ("\n^^^ is the tree before linearize\n\n")
      val stms = Canon.linearize body
      val _ = app (fn s => Printtree.printtree(out,s)) stms
      val _ = print ("\n^^^ is the tree after linearize\n\n")
      val stms' = Canon.traceSchedule(Canon.basicBlocks stms)
      val instrs = List.concat(map (MipsGen.codegen frame) stms')
      val instrs2 = F.procEntryExit2(frame,instrs)
      val (fg, node_list) = MakeGraph.instrs2graph(instrs2)
      val ig = Liveness.interferenceGraph(fg,node_list)
      val {prolog,body,epilog} = F.procEntryExit3(frame,instrs2)
      val final_instrs = addtab body
      val format0 = Assem.format(tempname)
  in
      app (fn i => TextIO.output(out,(format0 i)^"\n")) final_instrs
  end
  | emitproc out (F.STRING(lab,s)) = TextIO.output(out,F.string(lab,s))

val abs = Parse.parse "scratch_test.tig";
print "Finished parsing Abstract Syntax Tree.\n\n";
PrintAbsyn.print(TextIO.stdOut,abs); print "\n";

FindEscape.findEscape(abs);
print "Finished finding escapes.\n\n";

Semant.transProg(abs);
print "Finished Semant.transProg.\n\n";

val frags = Translate.getResult();

app (emitproc TextIO.stdOut) frags;
